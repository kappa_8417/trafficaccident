@extends("layout")

@section("content")
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">交通事故及勤務分布</h1>
</div>
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <div class="form-row align-items-center">
        <div class="col-sm-2 my-1">
            <input type="text" class="form-control" id="picker_start" placeholder="開始日期">
        </div>
        ~
        <div class="col-sm-2 my-1">
            <input type="text" class="form-control" id="picker_end" placeholder="結束日期">
        </div>
        <div class="col-auto my-1">
            <button id="update-google-map" class="btn btn-primary">篩選</button>
            <button id="reset-google-map" class="btn btn-danger">重置</button>
        </div>
        <div class="input-group flex-nowrap col-sm-2 my-1">
            <div class="input-group-prepend">
                <img class="input-group-text" src="/img/a1-pin-24.png" />
            </div>
            <input type="text" class="form-control" placeholder="A1 事故" disabled="true">
        </div>
        <div class="input-group flex-nowrap col-sm-2 my-1">
            <div class="input-group-prepend">
                <img class="input-group-text" src="/img/a2-pin-24.png" />
            </div>
            <input type="text" class="form-control" placeholder="A2 事故" disabled="true">
        </div>
        <div class="input-group flex-nowrap col-sm-2 my-1">
            <div class="input-group-prepend">
                <img class="input-group-text" src="/img/police-24.png" />
            </div>
            <input type="text" class="form-control" placeholder="員警值勤地點" disabled="true">
        </div>
    </div>

</div>
<!-- Content Row -->
<div class="row">
    <div id="map"></div>
</div>
<script>
    $.datetimepicker.setLocale("zh-TW");
        $('#picker_start').datetimepicker({
            timepicker: false,
            datepicker: true,
            format: 'Y/m/d',
            value: '2021/01/01',
        })
        $('#picker_end').datetimepicker({
            timepicker: false,
            datepicker: true,
            format: 'Y/m/d',
            value: '2021/12/31'
        })
</script>
<link href="css/googlemap.css" rel="stylesheet">
<script src="js/marker_googlemap.js"></script>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhFL3fnx84m8hANfALoNVa2ueQIHrzY4Y&callback=initMap&libraries=&v=weekly"
    async></script>
<!-- Content Row -->

@endsection("content")