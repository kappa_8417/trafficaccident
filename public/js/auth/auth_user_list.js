const url_location = "/api/auth/user_list";

var add_btn = document.getElementById('add_user');
add_btn.addEventListener('click', function () {

    let token = localStorage.getItem('jwt');

    var account = document.getElementById('account').value;
    var name = document.getElementById('name').value;
    var password = document.getElementById('password').value;
    var password_confirmation = document.getElementById('password_confirmation').value;
    var email = document.getElementById('email').value;

    if (password != password_confirmation) {
        swal("建立失敗", "請確認密碼是否正確", "error");
    } else {
        let formData = new FormData();
        formData.append('account', account); //required
        formData.append('name', name); //required
        formData.append('password', password); //required
        formData.append('password_confirmation', password); //required
        formData.append('email', email); //required

        axios({
            method: 'POST',
            url: url_location,
            headers: {
                "Accept": "application/json",
                "Authorization": "Bearer " + token
            },
            data: formData,
        }).then(function (response) {
            if (response.data.status == "Success") {

                myTable.ajax.url(url_location).load();

                $('#addModal').modal('toggle');

                document.getElementById('account').value = "";
                document.getElementById('name').value = "";
                document.getElementById('password').value = "";
                document.getElementById('password_confirmation').value = "";
                document.getElementById('email').value = "";

                swal("新增成功", "", "success");


            }
        }).catch(function (response) {

            if (response.response.data.message == "The given data was invalid.") {

                swal("新增失敗", "帳號已被使用", "error");

            }

        })

    }

});