const url_location = '/api/auth/login';

var login_btn = document.getElementById('login');
login_btn.addEventListener('click', function () {

    var name = document.getElementById('name').value;
    var password = document.getElementById('password').value;

    if (name == null || password == null) {
        swal("登入失敗", "請確認帳號密碼是否正確", "error");
        return false;
    } else {
        let formData = new FormData();
        formData.append('name', name); //required
        formData.append('password', password); //required

        axios({
            method: 'POST',
            url: url_location,
            data: formData,
            headers: { 'Content-Type': 'application/json' },
        }).then(function (response) {
            if (response.data.status == "Success") {

                localStorage.setItem('jwt', response.data.access_token);

                window.location = '/';

            }
        }).catch(function (response) {

            if (response.response.message == "Invalid Credentials") {

                swal("登入失敗", "請確認帳號密碼是否正確", "error");

            } else {

                swal("登入失敗", "請確認帳號密碼是否正確", "error");

            }

        })

    }

});