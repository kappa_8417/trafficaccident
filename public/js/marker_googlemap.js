var ta_json, patrol_json;
var a = -1;
var activeInfoWindow;
var map;
var ta_markers = [], patrol_markers = [];
// API 全資料路徑，此為常數
const url_location = "/api/ta/ta_log";
const url_patrol = "/api/ta/ta_patrol";

let token = localStorage.getItem('jwt');

// 設定「篩選」按鈕 on click 監聽器
var update_btn = document.getElementById('update-google-map');
update_btn.addEventListener('click', function () {

    // 取得 datetimepicker 的值
    var strat = document.getElementById('picker_start').value;
    var end = document.getElementById('picker_end').value;

    // 設定 API 存取條件
    var api_log = url_location + "?filter=case_date:" + strat + "~" + end;
    var api_patrol = url_patrol + "?filter=set_date:" + strat + "~" + end

    // 取得資料
    getTAData(api_log);
    getPatrolData(api_patrol);

});

// 設定「重置」按鈕 on click 監聽器
var reset_btn = document.getElementById('reset-google-map');
reset_btn.addEventListener('click', function () {

    // 取得資料
    getTAData(url_location);
    getPatrolData(url_patrol);

});

// 初始化地圖
function initMap() {

    // 第一次載入地圖的中心點
    uluru = { lat: 22.756675, lng: 121.15024 };

    // 載入地圖
    map = new google.maps.Map(document.getElementById("map"), {

        // 設定地圖顯示倍率
        zoom: 18,

        // 設定中心點
        center: uluru,
    });

    // 取得資料
    getTAData(url_location);
    getPatrolData(url_patrol);

}

// 取得事故資料
function getTAData(cache) {

    // 若有 Market 則先跑重置 Market
    if (ta_markers.length > 0) {
        for (var i = 0; i < ta_markers.length; i++) {
            ta_markers[i].setMap(null);
        }
        ta_markers = [];
    }

    // 使用 axios 取得 API 資料
    axios({
        url: cache,
        method: 'get',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
    })
        .then(function (response) {

            // 回傳資料放置於 json
            ta_json = response.data.data;

            // 根據資料長度設定 Market
            for (var i = 0; i < ta_json.length; i++) {
                addTAMarker(i);
            }
        });

}

// 設定事故 Market
function addTAMarker(e) {

    // 點下 Market 後要顯示的內容
    var log = "案件編號：" + ta_json[e].serial_number + "<br>發生時間：" + ta_json[e].case_datetime + "<br>肇事主因：" + ta_json[e].case_cause;

    // Market 座標
    var position = {
        lat: parseFloat(ta_json[e].latitude),
        lng: parseFloat(ta_json[e].longitude)
    };

    // 根據事故種類使用相對應顏色的 icon，A1=黃、A2=紅、A3=綠
    if (ta_json[e].accident_category == "A1") {
        ta_markers[e] = new google.maps.Marker({
            position: position,
            map: map,
            icon: {
                url: 'img/a1-pin-32.png'
            }
        });
    } else if (ta_json[e].accident_category == "A2") {
        ta_markers[e] = new google.maps.Marker({
            position: position,
            map: map,
            icon: {
                url: 'img/a2-pin-16.png'
            }
        });
    } else if (ta_json[e].accident_category == "A3") {
        ta_markers[e] = new google.maps.Marker({
            position: position,
            map: map,
            icon: {
                url: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
            }
        });
    }

    // 設定 Market 的文字內容
    var infowindow = new google.maps.InfoWindow({
        content: log,
        position: position,
        pixelOffset: new google.maps.Size(0, -5)
    });

    // 設定點下 Market 的監聽器
    ta_markers[e].addListener('click', function () {
        a = a * -1;
        if (a > 0) {
            if (activeInfoWindow) {
                activeInfoWindow.close();
            }
            infowindow.open(map, ta_markers[e]);
            activeInfoWindow = infowindow;
        } else {
            infowindow.close();
        }
    });
};

// 取得勤務點資料
function getPatrolData(cache) {

    // 若有 Market 則先跑重置 Market
    if (patrol_markers.length > 0) {
        for (var i = 0; i < patrol_markers.length; i++) {
            patrol_markers[i].setMap(null);
        }
        patrol_markers = [];
    }

    // 使用 axios 取得 API 資料
    axios({
        url: cache,
        method: 'get',
        headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        },
    })
        .then(function (response) {

            // 回傳資料放置於 json
            patrol_json = response.data.data;

            // 根據資料長度設定 Market
            for (var i = 0; i < patrol_json.length; i++) {

                if (patrol_json[i].latitude != null || patrol_json[i].longitude != null) {

                    addPatrolMarker(i);

                }

            }

        });

}

// 設定勤務點 Market
function addPatrolMarker(e) {

    // 點下 Market 後要顯示的內容
    var log = "值勤單位：" + patrol_json[e].team_name + "<br>值勤員警：" + patrol_json[e].police_name + "<br>值勤內容：" + patrol_json[e].patrol_content + "<br>值勤日期：" + patrol_json[e].set_date;

    // Market 座標
    var position = {
        lat: parseFloat(patrol_json[e].latitude),
        lng: parseFloat(patrol_json[e].longitude)
    };

    // 根據事故種類使用相對應顏色的 icon，A1=黃、A2=紅、A3=綠
    patrol_markers[e] = new google.maps.Marker({
        position: position,
        map: map,
        icon: {
            // url: 'img/police-pin.png'
            // url: 'img/traffic-lights-64.png'
            url: 'img/police-32.png'
        },
        zIndex: 99999999
    });

    // 設定 Market 的文字內容
    var infowindow = new google.maps.InfoWindow({
        content: log,
        position: position,
        pixelOffset: new google.maps.Size(0, -5)
    });

    // 設定點下 Market 的監聽器
    patrol_markers[e].addListener('click', function () {
        a = a * -1;
        if (a > 0) {
            if (activeInfoWindow) {
                activeInfoWindow.close();
            }
            infowindow.open(map, patrol_markers[e]);
            activeInfoWindow = infowindow;
        } else {
            infowindow.close();
        }
    });
};
