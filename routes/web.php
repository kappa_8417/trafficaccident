<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/login', function () {
    return view('auth/login');
})->name('login');

Route::middleware(['CheckToken'])->group(function () {
    Route::get('/', function () {
        return view('welcome');
    })->name('Index');

    Route::get('/InsertTA', function () {
        return view('InsertTA');
    })->name('InsertTA');

    Route::get('/InsertPatrol', function () {
        return view('InsertPatrol');
    })->name('InsertPatrol');

    Route::get('/MarkerMap', function () {
        return view('MarkerMap');
    })->name('MarkerMap');

    Route::get('/ChartMap', function () {
        return view('ChartMap');
    })->name('ChartMap');

    Route::get('/UserList', function () {
        return view('UserList');
    })->name('UserList');
});
